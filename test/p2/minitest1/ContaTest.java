package p2.minitest1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class ContaTest {
	
	private Conta conta;
	private final double SALDO_INICIAL = 100;
	
	@Before
	public void iniciaConta() {
		conta = new Conta("Maricota Silva", "425212", 100);
	}

	@Test
	public void testSaldo() {
		assertEquals(SALDO_INICIAL, conta.getSaldo(),0);
		conta.depositar(200);
		assertEquals(300, conta.getSaldo(),0);
	}
	
	@Test 
	public void testDepositarValorNegativo() {
		boolean resultado = conta.depositar(-100);
		assertTrue(!resultado);
		assertEquals(SALDO_INICIAL, conta.getSaldo(), 0);
	}
	
	@Test
	public void testSacarValorMaiorQueSaldo() {
		boolean resultado = conta.sacar(50);
		assertTrue(resultado);
		assertEquals(50, conta.getSaldo(), 0);
		
		resultado = conta.sacar(150);
		assertFalse(resultado);
		assertEquals(50, conta.getSaldo(), 0);
	}
	
	@Test
	public void testToString() {
		String resultadoEsperado = "Nome: Maricota Silva - "
				+ "CPF: 425212 - Saldo: 100.0";
		assertEquals(resultadoEsperado, conta.toString());
	}

}
