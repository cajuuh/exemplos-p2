package p2.minitest1;

public class Conta {

	private String nome;
	private String cpf;
	private double saldoSimples;
	private final double limite;
	
	public Conta(String nome, String cpf, double limite) {
		this.nome = nome;
		this.cpf = cpf;
		this.limite = limite;
	}
	
	public boolean depositar(double valor) {
		if (valor < 0)
			return false;
		
		saldoSimples += valor;
		return true;
	}
	
	public boolean sacar(double valor) {
		if (valor < 0 || valor > getSaldo())
			return false;
		
		saldoSimples -= valor;
		return true;
	}
	
	public double getSaldo() {
		return this.saldoSimples + this.limite;
	}
	
	public String toString() {
		return "Nome: " + this.nome + " - CPF: " + this.cpf +
				" - Saldo: " + getSaldo(); 
	}
	
	
}
