package p2.exemplos.facebook;

import java.util.Scanner;

public class Facebook {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		CadastroDeUsuario cadastro = new CadastroDeUsuario(4);
		
		boolean continua = true;
		int opcao;
		while (continua) {
			System.out.println("Digite uma op��o: ");
			System.out.println("1) Cadastrar");
			System.out.println("2) Login");
			System.out.println("3) Sair");
			opcao = sc.nextInt();
			
			switch (opcao) {
			case 1:
				cadastro(cadastro,sc);
				break;

			default:
				break;
			}
		}
	}
	
	private static void cadastro(CadastroDeUsuario cadastro, Scanner sc) {
		System.out.println("Digite o nome:");
		String nome = sc.next();
		System.out.println("Digite o email:");
		String email = sc.next();
		System.out.println("Digite a senha:");
		String senha = sc.next();
		
		Usuario usuario = new Usuario(nome,email,senha);
		try {
		cadastro.adicionaUsuario(usuario);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	
}
