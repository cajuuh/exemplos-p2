package p2.exemplos.facebook;

public class CadastroDeUsuario {
	
	private Usuario[] usuarios;
	private int indexUsuario;
	
	public CadastroDeUsuario(int quantidadeMaxima) {
		usuarios = new Usuario[quantidadeMaxima];
	}
	
	public void adicionaUsuario(Usuario usuario) throws Exception {
		if (usuarioJaExiste(usuario)) {
			throw new Exception("Usu�rio j� cadastrado");
		}
			
		usuarios[indexUsuario] = usuario;
		indexUsuario++;
	
	}
	
	private boolean usuarioJaExiste(Usuario usuario) {
		return buscaUsuarioPorEmail(usuario.getEmail()) != null;
	}
	
	
	
	public Usuario buscaUsuarioPorEmail(String email) {
		for (Usuario usuario : usuarios) {
			if (usuario != null && usuario.getEmail().equals(email))
				return usuario;
		}
		return null;
	}

}
