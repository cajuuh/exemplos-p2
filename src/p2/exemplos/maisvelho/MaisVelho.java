package p2.exemplos.maisvelho;

import java.util.Scanner;


public class MaisVelho {

	public static void main(String[] args) {

		String[] nomes = new String[3];
		int[] idades = new int[3];

		for (int indicePessoa = 0; indicePessoa < idades.length; indicePessoa++) {
			nomes[indicePessoa] = lerNome();
			idades[indicePessoa] = lerIdade();
		}
		
		imprimePessoas(nomes,idades);
		int indexMaisVelho = calculaMaisVelho(idades);
		System.out.println("O(a) mais velho(a) �: " + nomes[indexMaisVelho]);
	}
	
	private static int calculaMaisVelho(int[] idades) {
		int maior = 0;
		for (int index = 0; index < idades.length; index++) {
			maior = idades[index] >= idades[maior] ? index : maior;
		}
		return maior;
	}
 
	private static void imprimePessoas(String[] nomes, int[] idades) {
		for (int i = 0; i < nomes.length; i++) {
			System.out.println("Nome: " + nomes[i] + " Idade: " + idades[i]);
		}
	}

	public static String lerNome() {
		System.out.println("Digite o nome de uma pessoa: ");
		Scanner sc = new Scanner(System.in);
		return sc.next();
	}
	
	public static int lerIdade() {
		System.out.println("Digite a idade da pessoa: ");
		Scanner scanner = new Scanner(System.in);
		return scanner.nextInt();
	}

}
