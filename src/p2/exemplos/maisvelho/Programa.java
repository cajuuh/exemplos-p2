package p2.exemplos.maisvelho;

import java.util.Scanner;

import p2.exemplos.comum.Pessoa;

public class Programa {

	public static void main(String[] args) {
		Pessoa[] pessoas = new Pessoa[3];
		for (int indexPessoa = 0; indexPessoa < pessoas.length; indexPessoa++) {
			Pessoa pessoa = criaPessoa();
			pessoas[indexPessoa] = pessoa;
		}
		imprimePessoas(pessoas);
		imprimePessoaMaisVelha(pessoas);
	}

	private static void imprimePessoaMaisVelha(Pessoa[] pessoas) {
		
		System.out.println("A pessoa mais velha �: "
		+ Pessoa.pessoaMaisVelha(pessoas));
	}

	private static void imprimePessoas(Pessoa[] pessoas) {
		for (Pessoa pessoa : pessoas) {
			System.out.println(pessoa);
		}
	}
	
	public static Pessoa criaPessoa() {
		Scanner sc = new  Scanner(System.in);
		System.out.println("Digite o nome da pessoa: ");
		String nome = sc.next();
		System.out.println("Digite a idade da pessoa: ");
		int idade = sc.nextInt();
		Pessoa pessoa = new Pessoa(nome, idade);
		return pessoa;
	}
}
