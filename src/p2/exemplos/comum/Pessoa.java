package p2.exemplos.comum;


public class Pessoa {

	private int idade;
	private String nome;
	private String cpf;
	
	public Pessoa(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public Pessoa(String nome, int idade) {
		this.nome = nome;
		this.idade = idade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getCpf() {
		return this.cpf;
	}
	
	
	
	
	public boolean eMaisVelhaQue(Pessoa pessoa) {
		return this.idade > pessoa.idade;
	}

	public String toString() {
		return "Nome: " + this.nome + " Idade: " + this.idade;
	}
	
	public static Pessoa pessoaMaisVelha(Pessoa[] pessoas) {
		int indexMaior = 0;
		for (int indexPessoa = 1; indexPessoa < pessoas.length; indexPessoa++) {
			
			Pessoa pessoaAtual = pessoas[indexPessoa];
			if (pessoaAtual.eMaisVelhaQue(pessoas[indexMaior])) {
				indexMaior = indexPessoa;
			}
		}
		return pessoas[indexMaior];
	}
}
