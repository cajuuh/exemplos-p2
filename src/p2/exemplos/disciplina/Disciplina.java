package p2.exemplos.disciplina;

public class Disciplina {
	
	private double[] notas;
	private int notaIndex = 0;
	private String nome;
	
	
	public Disciplina(String nome) {
		
		this.nome = nome;
		this.notas = new double[3];
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public void adicionaNota(double nota) {
		this.notas[notaIndex] = nota;
		this.notaIndex++;
	}
	
	public double calculaMedia() {
		double soma = 0;
		
		for (double nota : this.notas) {
			soma += nota;
		}
		return soma / notas.length;
	}

}
